Matlab source code for the experiments reported in the paper:

Ferro, N. and Sanderson, M. (2017). Sub-corpora impact on system effectiveness. In Kando, N., Sakai, T., Joho, H., Li, H., de Vries, A. P., and White, R. W., editors, *Proc. 40th Annual International ACM SIGIR Conference on Research and Development in Information Retrieval (SIGIR 2017)*. ACM Press, New York, USA.
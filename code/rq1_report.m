%% rq1_report
% 
% Reports the different RQ1 analyses for the given
% track and saves them to a |.tex| file.
%
%% Synopsis
%
%   [] = rq1_plot_multcompare(trackID, splitID, startMeasure, endMeasure)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|splitID|* - the identifier of the split to process.
% * *|startMeasure|* - the index of the start measure to analyse. Optional.
% * *|endMeasure|* - the index of the end measure to analyse. Optional.

%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2015b or higher
% * *Copyright:* (C) 2016-2017 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = rq1_report(trackID, splitID, startMeasure, endMeasure)

    persistent TAG TAGa TAGb TAGc;
    
    if isempty(TAG)
        TAG = 'rq1';
        TAGa = 'rq1a';  % topic/system on whole corpus
        TAGb = 'rq1b';  % topic/system on sub-corpora
        TAGc = 'rq1c';  % topic/system/corpus on sub-corpora
    end;
    
    % check the number of input parameters
    narginchk(2, 4);

    % load common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
              
    % check that splitID is a non-empty cell array
    validateattributes(splitID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'splitID');
    
    if iscell(splitID)
        % check that splitID is a cell array of strings with one element
        assert(iscellstr(splitID) && numel(splitID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected splitID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    splitID = char(strtrim(splitID));
    splitID = splitID(:).';
    
    % check that splitID assumes a valid value
    validatestring(splitID, ...
        EXPERIMENT.split.list, '', 'splitID');

    if nargin == 4
        validateattributes(startMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number }, '', 'startMeasure');
        
        validateattributes(endMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startMeasure, '<=', EXPERIMENT.measure.number }, '', 'endMeasure');
        
    else
        startMeasure = 1;
        endMeasure = EXPERIMENT.measure.number;
    end;     
     
    % start of overall computations
    startComputation = tic;
    
    
    fprintf('\n\n######## Reporting %s ANOVA analysis on track %s (%s) ########\n\n', ...
        TAG, EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - analysis type:\n');
    fprintf('    * %s\n', TAG);
    fprintf('    * %s\n', EXPERIMENT.analysis.(TAG).name);
    fprintf('    * %s\n', EXPERIMENT.analysis.(TAG).description);
    fprintf('  - track %s\n', trackID);
    fprintf('  - split: %s\n', splitID);
    fprintf('  - measures:\n');
    fprintf('    * start measure: %d (%s)\n', startMeasure, EXPERIMENT.measure.getAcronym(startMeasure));
    fprintf('    * end measure: %d (%s)\n', endMeasure, EXPERIMENT.measure.getAcronym(endMeasure));
    fprintf('  - significance level alpha: %f\n', EXPERIMENT.analysis.alpha.threshold);

        
    fprintf('+ Printing the report\n');
    
    % the file where the report has to be written
    %reportID = EXPERIMENT.pattern.identifier.anova.rep(TAG, strjoin(EXPERIMENT.measure.list(startMeasure:endMeasure), '_'), splitID, trackID);
    reportID = EXPERIMENT.pattern.identifier.anova.rep(TAG, splitID, trackID);
    fid = fopen(EXPERIMENT.pattern.file.report(trackID, reportID), 'w');


    fprintf(fid, '\\documentclass[11pt]{article} \n\n');

    fprintf(fid, '\\usepackage{amsmath}\n');
    fprintf(fid, '\\usepackage{multirow}\n');
    fprintf(fid, '\\usepackage{colortbl}\n');
    fprintf(fid, '\\usepackage{lscape}\n');
    fprintf(fid, '\\usepackage{pdflscape}\n');
    fprintf(fid, '\\usepackage{rotating}\n');
    fprintf(fid, '\\usepackage[a3paper]{geometry}\n\n');
    
    fprintf(fid, '\\usepackage{xcolor}\n');
    fprintf(fid, '\\definecolor{lightgrey}{RGB}{219, 219, 219}\n');
    fprintf(fid, '\\definecolor{verylightblue}{RGB}{204, 229, 255}\n');
    fprintf(fid, '\\definecolor{lightblue}{RGB}{124, 216, 255}\n');
    fprintf(fid, '\\definecolor{blue}{RGB}{32, 187, 253}\n');
    
    fprintf(fid, '\\begin{document}\n\n');
    
    fprintf(fid, '\\title{Summary Report on Topic/System/Corpus ANOVA Analyses \\\\ on %s}\n\n', EXPERIMENT.track.(trackID).name);
    
    fprintf(fid, '\\author{Nicola Ferro}\n\n');
    
    fprintf(fid, '\\maketitle\n\n');
    
    
    fprintf(fid, 'Split %s, %s, consists of the following corpora:\n', strrep(splitID, '_', '\_'), EXPERIMENT.split.(splitID).name);
    fprintf(fid, '\\begin{itemize}\n');
    
    % for each sub-corpus
    for c = 1:length(EXPERIMENT.split.(splitID).corpus)
        
        corpusID = EXPERIMENT.split.(splitID).corpus{c};
        
        fprintf(fid, '\\item %s: %s; %d documents\n', ...
            strrep(corpusID, '_', '\_'), EXPERIMENT.corpus.(corpusID).name, EXPERIMENT.corpus.(corpusID).size);
        
    end; % for sub-corpus
    
    fprintf(fid, '\\end{itemize}\n');
        
        
    
    fprintf(fid, '\\vspace*{1em}Rule of thumb for effect size $\\hat{\\omega}_{\\langle fact\\rangle}^2$:\n');
    fprintf(fid, '\\begin{itemize}\n');    
    fprintf(fid, '\\item large effect: $\\hat{\\omega}_{\\langle fact\\rangle}^2 \\geq 0.14$\n');
    fprintf(fid, '\\item medium effect: $0.06 \\leq \\hat{\\omega}_{\\langle fact\\rangle}^2 < 0.14$\n');
    fprintf(fid, '\\item small effect: $0.01 \\leq \\hat{\\omega}_{\\langle fact\\rangle}^2 < 0.06$\n');
    fprintf(fid, '\\item negative values have to be considered as $0$\n');
    fprintf(fid, '\\end{itemize}\n');
    
    
     fprintf(fid, '\\vspace*{1em}Analysed measures:\n');
    fprintf(fid, '\\begin{itemize}\n');
    
    % for each measure
    for m = startMeasure:endMeasure        
        fprintf(fid, '\\item %s: %s\n', ...
            EXPERIMENT.measure.getAcronym(m), EXPERIMENT.measure.getName(m));        
    end; % for measure
    
    fprintf(fid, '\\end{itemize}\n');
    
        
    fprintf(fid, '\\newpage\n');
  
    % for each measure
    for m = startMeasure:endMeasure
        
        mid = EXPERIMENT.measure.list{m};
        
        % topic/system on whole corpus
        anovaID = EXPERIMENT.pattern.identifier.anova.analysis(TAGa, mid, splitID, trackID);
        anovaTableID = EXPERIMENT.pattern.identifier.anova.tbl(TAGa, mid, splitID, trackID);
        anovaSoAID = EXPERIMENT.pattern.identifier.anova.soa(TAGa, mid, splitID, trackID);
        
        serload2(EXPERIMENT.pattern.file.analysis(trackID, anovaID), ...
                'WorkspaceVarNames', {'tblA', 'soaA'}, ...
                'FileVarNames', {anovaTableID, anovaSoAID});
            
        % topic/system on sub-corpora
        anovaID = EXPERIMENT.pattern.identifier.anova.analysis(TAGb, mid, splitID, trackID);
        anovaTableID = EXPERIMENT.pattern.identifier.anova.tbl(TAGb, mid, splitID, trackID);
        anovaSoAID = EXPERIMENT.pattern.identifier.anova.soa(TAGb, mid, splitID, trackID);
        
        serload2(EXPERIMENT.pattern.file.analysis(trackID, anovaID), ...
                'WorkspaceVarNames', {'tblB', 'soaB'}, ...
                'FileVarNames', {anovaTableID, anovaSoAID});
            
            
        % topic/system/corpus on sub-corpora
        anovaID = EXPERIMENT.pattern.identifier.anova.analysis(TAGc, mid, splitID, trackID);
        anovaTableID = EXPERIMENT.pattern.identifier.anova.tbl(TAGc, mid, splitID, trackID);
        anovaSoAID = EXPERIMENT.pattern.identifier.anova.soa(TAGc, mid, splitID, trackID);
        
        serload2(EXPERIMENT.pattern.file.analysis(trackID, anovaID), ...
                'WorkspaceVarNames', {'tblC', 'soaC'}, ...
                'FileVarNames', {anovaTableID, anovaSoAID});

            
        % topic/system on whole corpus
        fprintf(fid, '\\begin{table}[h] \n');
        % fprintf(fid, '\\tiny \n');
        fprintf(fid, '\\centering \n');
        %fprintf(fid, '\\hspace*{-6.5em} \n');

        fprintf(fid, '\\begin{tabular}{|l|r|r|r|r|r|r|} \n');
        
        fprintf(fid, '\\hline \n');
        
        fprintf(fid, '\\multicolumn{1}{|c}{\\textbf{Source}} & \\multicolumn{1}{|c}{\\textbf{SS}} & \\multicolumn{1}{|c}{\\textbf{DF}} & \\multicolumn{1}{|c}{\\textbf{MS}} & \\multicolumn{1}{|c}{\\textbf{F}} & \\multicolumn{1}{|c}{\\textbf{p-value}} & \\multicolumn{1}{|c|}{$\\hat{\\omega}_{\\langle fact\\rangle}^2$} \\\\ \n');
        
        fprintf(fid, '\\hline \n');
        
        fprintf(fid, '\\textbf{Topic}					& %10.4f			& %d				& %10.4f		& %10.4f	& %.4g		& %10.4f \\\\', ...
            tblA{2, 2}, tblA{2, 3}, tblA{2, 5}, tblA{2, 6}, tblA{2, 7}, soaA.omega2p.topic);
        
        fprintf(fid, '\\hline \n');
        
        fprintf(fid, '\\textbf{System}					& %10.4f			& %d				& %10.4f		& %10.4f	& %.4g		& %10.4f \\\\', ...
            tblA{3, 2}, tblA{3, 3}, tblA{3, 5}, tblA{3, 6}, tblA{3, 7}, soaA.omega2p.system);
        
        fprintf(fid, '\\hline \n');
                
        fprintf(fid, '\\textbf{Error}                   & %10.4f			& %d				& %10.4f		&       	&      		&  \\\\', ...
            tblA{4, 2}, tblA{4, 3}, tblA{4, 5});
        
        fprintf(fid, '\\hline \n');
        
        fprintf(fid, '\\textbf{Total}                   & %10.4f			& %d				&       		&       	&      		&  \\\\', ...
            tblA{5, 2}, tblA{5, 3});
        
        fprintf(fid, '\\hline \n');
        
        fprintf(fid, '\\end{tabular} \n');

        fprintf(fid, '\\caption{Model $Y_{ij} = \\mu_{\\cdot\\cdot} + \\tau_i + \\alpha_j + \\varepsilon_{ij}$ for %s using whole collection on track \\texttt{%s}.}\n', ...
            EXPERIMENT.measure.getAcronym(m), trackID);
            
        fprintf(fid, '\\label{tab:%s}\n', anovaID);

        fprintf(fid, '\\end{table} \n\n');
        
                    
        % topic/system on sub-corpora
        fprintf(fid, '\\begin{table}[h] \n');
        % fprintf(fid, '\\tiny \n');
        fprintf(fid, '\\centering \n');
        %fprintf(fid, '\\hspace*{-6.5em} \n');

        fprintf(fid, '\\begin{tabular}{|l|r|r|r|r|r|r|} \n');
        
        fprintf(fid, '\\hline \n');
        
        fprintf(fid, '\\multicolumn{1}{|c}{\\textbf{Source}} & \\multicolumn{1}{|c}{\\textbf{SS}} & \\multicolumn{1}{|c}{\\textbf{DF}} & \\multicolumn{1}{|c}{\\textbf{MS}} & \\multicolumn{1}{|c}{\\textbf{F}} & \\multicolumn{1}{|c}{\\textbf{p-value}} & \\multicolumn{1}{|c|}{$\\hat{\\omega}_{\\langle fact\\rangle}^2$} \\\\ \n');
        
        fprintf(fid, '\\hline \n');
        
        fprintf(fid, '\\textbf{Topic}					& %10.4f			& %d				& %10.4f		& %10.4f	& %.4g		& %10.4f \\\\', ...
            tblB{2, 2}, tblB{2, 3}, tblB{2, 5}, tblB{2, 6}, tblB{2, 7}, soaB.omega2p.topic);
        
        fprintf(fid, '\\hline \n');
        
        fprintf(fid, '\\textbf{System}					& %10.4f			& %d				& %10.4f		& %10.4f	& %.4g		& %10.4f \\\\', ...
            tblB{3, 2}, tblB{3, 3}, tblB{3, 5}, tblB{3, 6}, tblB{3, 7}, soaB.omega2p.system);
        
        fprintf(fid, '\\hline \n');
                
        fprintf(fid, '\\textbf{Error}                   & %10.4f			& %d				& %10.4f		&       	&      		&  \\\\', ...
            tblB{4, 2}, tblB{4, 3}, tblB{4, 5});
        
        fprintf(fid, '\\hline \n');
        
        fprintf(fid, '\\textbf{Total}                   & %10.4f			& %d				&       		&       	&      		&  \\\\', ...
            tblB{5, 2}, tblB{5, 3});
        
        fprintf(fid, '\\hline \n');
        
        fprintf(fid, '\\end{tabular} \n');

        fprintf(fid, '\\caption{Model $Y_{ij} = \\mu_{\\cdot\\cdot} + \\tau_i + \\alpha_j + \\varepsilon_{ij}$ for %s using sub-corpora on track \\texttt{%s}.}\n', ...
            EXPERIMENT.measure.getAcronym(m), trackID);
            
        fprintf(fid, '\\label{tab:%s}\n', anovaID);

        fprintf(fid, '\\end{table} \n\n');
            
        
        % topic/system/corpus on sub-corpora
        fprintf(fid, '\\begin{table}[h] \n');
        % fprintf(fid, '\\tiny \n');
        fprintf(fid, '\\centering \n');
        %fprintf(fid, '\\hspace*{-6.5em} \n');

        fprintf(fid, '\\begin{tabular}{|l|r|r|r|r|r|r|} \n');
        
        fprintf(fid, '\\hline \n');
        
        fprintf(fid, '\\multicolumn{1}{|c}{\\textbf{Source}} & \\multicolumn{1}{|c}{\\textbf{SS}} & \\multicolumn{1}{|c}{\\textbf{DF}} & \\multicolumn{1}{|c}{\\textbf{MS}} & \\multicolumn{1}{|c}{\\textbf{F}} & \\multicolumn{1}{|c}{\\textbf{p-value}} & \\multicolumn{1}{|c|}{$\\hat{\\omega}_{\\langle fact\\rangle}^2$} \\\\ \n');
        
        fprintf(fid, '\\hline \n');
        
        fprintf(fid, '\\textbf{Topic}					& %10.4f			& %d				& %10.4f		& %10.4f	& %.4g		& %10.4f \\\\', ...
            tblC{2, 2}, tblC{2, 3}, tblC{2, 5}, tblC{2, 6}, tblC{2, 7}, soaC.omega2p.topic);
        
        fprintf(fid, '\\hline \n');
        
        fprintf(fid, '\\textbf{System}					& %10.4f			& %d				& %10.4f		& %10.4f	& %.4g		& %10.4f \\\\', ...
            tblC{3, 2}, tblC{3, 3}, tblC{3, 5}, tblC{3, 6}, tblC{3, 7}, soaC.omega2p.system);
        
        fprintf(fid, '\\hline \n');
        
        fprintf(fid, '\\textbf{Sub-Corpus}				& %10.4f			& %d				& %10.4f		& %10.4f	& %.4g		& %10.4f \\\\', ...
            tblC{4, 2}, tblC{4, 3}, tblC{4, 5}, tblC{4, 6}, tblC{4, 7}, soaC.omega2p.corpus);
        
        fprintf(fid, '\\hline \n');
        
        fprintf(fid, '\\textbf{Sub-Corpus*System}		& %10.4f			& %d				& %10.4f		& %10.4f	& %.4g		& %10.4f \\\\', ...
            tblC{5, 2}, tblC{5, 3}, tblC{5, 5}, tblC{5, 6}, tblC{5, 7}, soaC.omega2p.system_corpus);
        
        fprintf(fid, '\\hline \n');
        
        fprintf(fid, '\\textbf{Error}                   & %10.4f			& %d				& %10.4f		&       	&      		&  \\\\', ...
            tblC{6, 2}, tblC{6, 3}, tblC{6, 5});
        
        fprintf(fid, '\\hline \n');
        
        fprintf(fid, '\\textbf{Total}                   & %10.4f			& %d				&       		&       	&      		&  \\\\', ...
            tblC{7, 2}, tblC{7, 3});
        
        fprintf(fid, '\\hline \n');
        
        fprintf(fid, '\\end{tabular} \n');

        fprintf(fid, '\\caption{Model $Y_{ijk} = \\mu_{\\cdot\\cdot\\cdot} + \\tau_i + \\alpha_j + \\beta_k + (\\alpha\\beta)_{jk} + \\varepsilon_{ijk}$ for %s using sub-corpora on track \\texttt{%s}.}\n', ...
            EXPERIMENT.measure.getAcronym(m), trackID);

        fprintf(fid, '\\label{tab:%s}\n', anovaID);

        fprintf(fid, '\\end{table} \n\n');


        
    end; % for measure
    
    
    fprintf(fid, '\\end{document} \n\n');
    
    
    fclose(fid);
    
    
               
    fprintf('\n\n######## Total elapsed time for reporting %s ANOVA analysis on track %s (%s): %s ########\n\n', ...
            TAG, EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

    diary off;
end

%% split_runs
% 
% Splits a set of runs into different sets of runs, each one corresponding 
% to one of the subcorpora of the specified split.
%
%% Synopsis
%
%   [] = split_runs(trackID, splitID)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track to process.
% * *|splitID|* - the identifier of the split to process.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2015b or higher
% * *Copyright:* (C) 2016-2017 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%
function [] = split_runs(trackID, splitID)

    persistent PROCESS_RUN;
    
    if isempty(PROCESS_RUN)        
        PROCESS_RUN = @processRun;
    end;

    % check that we have the correct number of input arguments. 
    narginchk(2, 2);
    
    % setup common parameters
    common_parameters;
        
    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');    
    
    % check that splitID is a non-empty string
    validateattributes(splitID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'splitID');

     if iscell(splitID)
        % check that splitID is a cell array of strings with one element
        assert(iscellstr(splitID) && numel(splitID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected splitID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    splitID = char(strtrim(splitID));
    splitID = splitID(:).';
    
    % check that splitID assumes a valid value
    validatestring(splitID, ...
        EXPERIMENT.split.list, '', 'splitID');        

    % check that the track and the split rely on the same corpus
    assert(strcmp(EXPERIMENT.track.(trackID).corpus, EXPERIMENT.split.(splitID).partOf), 'Track %s and split %s do not rely on the same corpus', trackID, splitID);
        
    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Splitting runs for track %s (%s) ########\n\n', EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - split: %s\n', splitID);


    fprintf('+ Loading the runs\n');
        
    runID = EXPERIMENT.pattern.identifier.run(EXPERIMENT.split.(splitID).partOf, trackID);
    fprintf('  - %s\n', runID);
    
    serload2(EXPERIMENT.pattern.file.dataset(trackID, runID), ...
        'WorkspaceVarNames', {'partOfRun'}, ...
        'FileVarNames', {runID});
    
    % the total number of topics in the set
    T = height(partOfRun);
    
    % the total number of runs in the set
    R = width(partOfRun);
    
    fprintf('+ Processing sub-corpora\n');
    
    for s = 1:length(EXPERIMENT.split.(splitID).corpus)
    
        start = tic;
        
        corpusID = EXPERIMENT.split.(splitID).corpus{s};
        
        fprintf('  - sub-corpus %s\n', corpusID);
        
        fprintf('    * loading sub-corpus\n');
        
        corpus = importdata(EXPERIMENT.pattern.file.corpus(corpusID));
        
        % improves subsequent lookup operations
        corpus = sort(corpus);
        
        % replicate the subcorpus to properly work with cellfun
        corpus = repmat({corpus}, 1, R);
        
        fprintf('    * computing sub-corpus runs\n');
        
        corpusRun = partOfRun;
        
        for t = 1:T            
            corpusRun{t, :} = cellfun(PROCESS_RUN, corpusRun{t, :}, corpus, 'UniformOutput', false);            
        end; % for topic
        
        
        runID = EXPERIMENT.pattern.identifier.run(corpusID, trackID);
        corpusRun.Properties.UserData.identifier = runID;
                
        fprintf('    * saving subcorpus runs\n');
        
        sersave2(EXPERIMENT.pattern.file.dataset(trackID, runID), ...
            'WorkspaceVarNames', {'corpusRun'}, ...
            'FileVarNames', {runID});
        
        clear corpus corpusRun
        
        fprintf('    * elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
        
    end; % for sub-corpus
    
    fprintf('\n\n######## Total elapsed time for splitting runs for track %s (%s): %s ########\n\n', ...
            EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
    
end

function [subcorpusTopic] = processRun(topic, subcorpus)

    persistent DUMMY_TABLE;
   
    if isempty(DUMMY_TABLE)        
        DUMMY_TABLE = cell2table({'#####DUMMY_DOC_ID^^^^^^', 1, 1});
        DUMMY_TABLE.Properties.VariableNames = {'Document', 'Rank', 'Score'};        
    end;
    
    % find which rows (documents) have to be kept
    keep = ismember(topic.Document, subcorpus);
    
    % if there is at least one document, use it, otherwise use a default
    % dummy document, which will be not relevant when assessed.
    if any(keep)
        subcorpusTopic = topic(keep, :);
    else
        subcorpusTopic = DUMMY_TABLE;
    end;
end

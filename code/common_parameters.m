%% common_parameters
% 
% Sets up parameters common to the different scripts.
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2015b or higher
% * *Copyright:* (C) 2017 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

diary off;

%% Path Configuration

% if we are running on the cluster 
if (strcmpi(computer, 'GLNXA64'))
    addpath('/nas1/promise/ims/ferro/matters/base/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/analysis/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/io/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/measure/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/plot/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/util/')
end;

% The base path
if (strcmpi(computer, 'GLNXA64')) % if we are running on the cluster    
    EXPERIMENT.path.base = '/nas1/promise/ims/ferro/Sanderson/experiment/';
else % if we are running on the local machine
    EXPERIMENT.path.base = '/Users/ferro/Documents/pubblicazioni/2017/SIGIR2017/Sanderson/experiment/';
end;

% The path for the sub-corpora, i.e. list of documents from a corpus
% according to some criterion
EXPERIMENT.path.corpus = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'corpus', filesep);

% The path for the datasets, i.e. the runs and the pools of both original
% tracks and sub-corpora
EXPERIMENT.path.dataset = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'dataset', filesep);

% The path for the measures
EXPERIMENT.path.measure = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'measure', filesep);

% The path for samples of systems and topics
EXPERIMENT.path.samples = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'samples', filesep);

% The path for analyses
EXPERIMENT.path.analysis = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'analysis', filesep);

% The path for figures
EXPERIMENT.path.figure = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'figure', filesep);

% The path for reports
EXPERIMENT.path.report = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'report', filesep);

%% General Configuration

% Label of the paper this experiment is for
EXPERIMENT.label.paper = 'SIGIR 2017 Ferro and Sanderson';

%% Configuration for Corpora

EXPERIMENT.corpus.list = {'TIPSTER', 'TIPFBIS', 'TIPFR', 'TIPFT', 'TIPLA'};
EXPERIMENT.corpus.number = length(EXPERIMENT.corpus.list);
EXPERIMENT.corpus.name = 'Corpus';

% The full TIPSTER corpus
EXPERIMENT.corpus.TIPSTER.id = 'TIPSTER';
EXPERIMENT.corpus.TIPSTER.name = 'TIPSTER, Disk 4-5 minus Congressional Record';
EXPERIMENT.corpus.TIPSTER.size = 528155;

% The TIPSTER Foreign Broadcast Information Service corpus
EXPERIMENT.corpus.TIPFBIS.id = 'TIPFBIS';
EXPERIMENT.corpus.TIPFBIS.name = 'TIPSTER, Foreign Broadcast Information Service';
EXPERIMENT.corpus.TIPFBIS.partOf = 'TIPSTER';
EXPERIMENT.corpus.TIPFBIS.size = 130471;

% The TIPSTER Financial Register corpus
EXPERIMENT.corpus.TIPFR.id = 'TIPFR';
EXPERIMENT.corpus.TIPFR.name =  'TIPSTER, Financial Register';
EXPERIMENT.corpus.TIPFR.partOf = 'TIPSTER';
EXPERIMENT.corpus.TIPFR.size = 55630;

% The TIPSTER Financial Times corpus
EXPERIMENT.corpus.TIPFT.id = 'TIPFT';
EXPERIMENT.corpus.TIPFT.name = 'TIPSTER, Financial Times';
EXPERIMENT.corpus.TIPFT.partOf = 'TIPSTER';
EXPERIMENT.corpus.TIPFT.size = 210158;

% The TIPSTER Los Angeles Times corpus
EXPERIMENT.corpus.TIPLA.id = 'TIPLA';
EXPERIMENT.corpus.TIPLA.name = 'TIPSTER, Los Angeles Times';
EXPERIMENT.corpus.TIPLA.partOf = 'TIPSTER';
EXPERIMENT.corpus.TIPLA.size = 131896;


%% Configuration for Splits

EXPERIMENT.split.list = {'TIPDS'};
EXPERIMENT.split.number = length(EXPERIMENT.split.list);


% TIPSTER divided by document source
EXPERIMENT.split.TIPDS.id = 'TIPDS';
EXPERIMENT.split.TIPDS.name =  'TIPSTER divided by document source';
EXPERIMENT.split.TIPDS.partOf = 'TIPSTER';
EXPERIMENT.split.TIPDS.corpus = {'TIPFBIS', 'TIPFR', 'TIPFT', 'TIPLA'};


%% Configuration for Tracks

EXPERIMENT.track.list = {'T07', 'T08'};
EXPERIMENT.track.number = length(EXPERIMENT.track.list);

% TREC 07, 1998, Adhoc
EXPERIMENT.track.T07.id = 'T07';
EXPERIMENT.track.T07.name = 'TREC 07, 1998, Adhoc';
EXPERIMENT.track.T07.corpus = 'TIPSTER';

% TREC 08, 1999, Adhoc
EXPERIMENT.track.T08.id = 'T08';
EXPERIMENT.track.T08.name = 'TREC 08, 1999, Adhoc';
EXPERIMENT.track.T08.corpus = 'TIPSTER';

% Returns the name of a track given its index in EXPERIMENT.track.list
EXPERIMENT.track.getName = @(idx) ( EXPERIMENT.track.(EXPERIMENT.track.list{idx}).name ); 


%% Patterns for file names

% TXT - Pattern EXPERIMENT.path.base/corpus/<corpusID>.txt
EXPERIMENT.pattern.file.corpus = @(corpusID) sprintf('%1$s%2$s.txt', EXPERIMENT.path.corpus, corpusID);


% MAT - Pattern <path>/<trackID>/<fileID>.<ext>
EXPERIMENT.pattern.file.general = @(path, trackID, fileID, ext) sprintf('%1$s%2$s%3$s%4$s.%5$s', path, trackID, filesep, fileID, ext);

% MAT - Pattern EXPERIMENT.path.base/dataset/<trackID>/<datasetID>.mat
EXPERIMENT.pattern.file.dataset = @(trackID, datasetID) EXPERIMENT.pattern.file.general(EXPERIMENT.path.dataset, trackID, datasetID, 'mat');

% MAT - Pattern EXPERIMENT.path.base/measure/<trackID>/<measureID>.mat
EXPERIMENT.pattern.file.measure = @(trackID, measureID) EXPERIMENT.pattern.file.general(EXPERIMENT.path.measure, trackID, measureID, 'mat');

% MAT - Pattern EXPERIMENT.path.base/analysis/<trackID>/<analysisID>.mat
EXPERIMENT.pattern.file.analysis = @(trackID, analysisID) EXPERIMENT.pattern.file.general(EXPERIMENT.path.analysis, trackID, analysisID, 'mat');

% PDF - Pattern EXPERIMENT.path.base/figure/<trackID>/<figureID>
EXPERIMENT.pattern.file.figure = @(trackID, figureID) EXPERIMENT.pattern.file.general(EXPERIMENT.path.figure, trackID, figureID, 'pdf');

% TEX - Pattern EXPERIMENT.path.base/report/<trackID>/<reportID>
EXPERIMENT.pattern.file.report = @(trackID, reportID) EXPERIMENT.pattern.file.general(EXPERIMENT.path.report, trackID, reportID, 'tex');


%% Patterns for identifiers

% Pattern pool_<corpusID>_<trackID>
EXPERIMENT.pattern.identifier.pool =  @(corpusID, trackID) sprintf('pool_%1$s_%2$s', corpusID, trackID);

% Pattern run_<corpusID>_<trackID>
EXPERIMENT.pattern.identifier.run = @(corpusID, trackID) sprintf('run_%1$s_%2$s', corpusID, trackID);

% Pattern <measureID>_<corpusID>_<trackID>
EXPERIMENT.pattern.identifier.measure =  @(measureID, corpusID, trackID) sprintf('%1$s_%2$s_%3$s', measureID, corpusID, trackID);

% Pattern <rqID>_<type>_<measureID>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.general =  @(rqID, type, measureID, splitID, trackID) sprintf('%1$s_%2$s_%3$s_%4$s_%5$s', rqID, type, measureID, splitID, trackID);

% Pattern <rqID>_anova_<measureID>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.analysis =  @(rqID, measureID, splitID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'anova', measureID, splitID, trackID);

% Pattern <rqID>_obs_<measureID>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.obs =  @(rqID, measureID, splitID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'obs', measureID, splitID, trackID);

% Pattern <rqID>_tbl_<measureID>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.tbl =  @(rqID, measureID, splitID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'tbl', measureID, splitID, trackID);

% Pattern <rqID>_sts_<measureID>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.sts =  @(rqID, measureID, splitID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'sts', measureID, splitID, trackID);

% Pattern <rqID>_me_<measureID>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.me =  @(rqID, measureID, splitID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'me', measureID, splitID, trackID);

% Pattern <rqID>_ie_<measureID>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.ie =  @(rqID, measureID, splitID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'ie', measureID, splitID, trackID);

% Pattern <rqID>_mie_<measureID>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.mie =  @(rqID, measureID, splitID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'mie', measureID, splitID, trackID);

% Pattern <rqID>_mc_<measureID>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.mc =  @(rqID, measureID, splitID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'mc', measureID, splitID, trackID);

% Pattern <rqID>_soa_<measureID>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.soa =  @(rqID, measureID, splitID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'soa', measureID, splitID, trackID);

% Pattern <rqID>_rep_<splitID>_<trackID>
%EXPERIMENT.pattern.identifier.anova.rep =  @(rqID, measureID, corpusID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'rep', measureID, corpusID, trackID);
EXPERIMENT.pattern.identifier.anova.rep =  @(rqID, splitID, trackID) sprintf('%1$s_%2$s_%3$s_%4$s', rqID, 'rep', splitID, trackID);



%% Configuration for measures

% The list of measures under experimentation
EXPERIMENT.measure.list = {'ap', 'p10', 'rprec', 'rbp', 'ndcg', 'ndcg20', 'err', 'twist'};
EXPERIMENT.measure.number = length(EXPERIMENT.measure.list);

% Configuration for AP
EXPERIMENT.measure.ap.id = 'ap';
EXPERIMENT.measure.ap.acronym = 'AP';
EXPERIMENT.measure.ap.name = 'Average Precision';
EXPERIMENT.measure.ap.compute = @(pool, runSet, shortNameSuffix) averagePrecision(pool, runSet, 'ShortNameSuffix', shortNameSuffix);

% Configuration for P@10
EXPERIMENT.measure.p10.id = 'p10';
EXPERIMENT.measure.p10.acronym = 'P@10';
EXPERIMENT.measure.p10.name = 'Precision at 10 Retrieved Documents';
EXPERIMENT.measure.p10.cutoffs = 10;
EXPERIMENT.measure.p10.compute = @(pool, runSet, shortNameSuffix) precision(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'Cutoffs', EXPERIMENT.measure.p10.cutoffs);

% Configuration for R-prec
EXPERIMENT.measure.rprec.id = 'rprec';
EXPERIMENT.measure.rprec.acronym = 'R-prec';
EXPERIMENT.measure.rprec.name = 'Precision at the Recall Base';
EXPERIMENT.measure.rprec.compute = @(pool, runSet, shortNameSuffix) precision(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'Rprec', true);

% Configuration for RBP
EXPERIMENT.measure.rbp.id = 'rbp';
EXPERIMENT.measure.rbp.acronym = 'RBP';
EXPERIMENT.measure.rbp.name = 'Rank-biased Precision';
EXPERIMENT.measure.rbp.persistence = 0.8;
EXPERIMENT.measure.rbp.compute = @(pool, runSet, shortNameSuffix) rankBiasedPrecision(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'Persistence', EXPERIMENT.measure.rbp.persistence);

% Configuration for nDCG
EXPERIMENT.measure.ndcg.id = 'ndcg';
EXPERIMENT.measure.ndcg.acronym = 'nDCG';
EXPERIMENT.measure.ndcg.name = 'Normalized Discounted Cumulated Gain at Last Retrieved Document';
EXPERIMENT.measure.ndcg.cutoffs = 'LastRelevantRetrieved';
EXPERIMENT.measure.ndcg.logBase = 10;
EXPERIMENT.measure.ndcg.fixNumberRetrievedDocuments = 1000;
EXPERIMENT.measure.ndcg.fixedNumberRetrievedDocumentsPaddingStrategy = 'NotRelevant';
EXPERIMENT.measure.ndcg.compute = @(pool, runSet, shortNameSuffix) discountedCumulatedGain(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'CutOffs', EXPERIMENT.measure.ndcg.cutoffs, 'LogBase', EXPERIMENT.measure.ndcg.logBase, 'Normalize', true, 'FixNumberRetrievedDocuments', EXPERIMENT.measure.ndcg.fixNumberRetrievedDocuments, 'FixedNumberRetrievedDocumentsPaddingStrategy', EXPERIMENT.measure.ndcg.fixedNumberRetrievedDocumentsPaddingStrategy);

% Configuration for nDCG@20
EXPERIMENT.measure.ndcg20.id = 'ndcg20';
EXPERIMENT.measure.ndcg20.acronym = 'nDCG@20';
EXPERIMENT.measure.ndcg20.name = 'Normalized Discounted Cumulated Gain at 20 Retrieved Documents';
EXPERIMENT.measure.ndcg20.cutoffs = 20;
EXPERIMENT.measure.ndcg20.logBase = 10;
EXPERIMENT.measure.ndcg20.fixNumberRetrievedDocuments = 1000;
EXPERIMENT.measure.ndcg20.fixedNumberRetrievedDocumentsPaddingStrategy = 'NotRelevant';
EXPERIMENT.measure.ndcg20.compute = @(pool, runSet, shortNameSuffix) discountedCumulatedGain(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'CutOffs', EXPERIMENT.measure.ndcg20.cutoffs, 'LogBase', EXPERIMENT.measure.ndcg20.logBase, 'Normalize', true, 'FixNumberRetrievedDocuments', EXPERIMENT.measure.ndcg20.fixNumberRetrievedDocuments, 'FixedNumberRetrievedDocumentsPaddingStrategy', EXPERIMENT.measure.ndcg20.fixedNumberRetrievedDocumentsPaddingStrategy);

% Configuration for ERR
EXPERIMENT.measure.err.id = 'err';
EXPERIMENT.measure.err.acronym = 'ERR';
EXPERIMENT.measure.err.name = 'Expected Reciprocal Rank at Last Retrieved Document';
EXPERIMENT.measure.err.cutoffs = 'LastRelevantRetrieved';
EXPERIMENT.measure.err.fixNumberRetrievedDocuments = 1000;
EXPERIMENT.measure.err.fixedNumberRetrievedDocumentsPaddingStrategy = 'NotRelevant';
EXPERIMENT.measure.err.compute = @(pool, runSet, shortNameSuffix) expectedReciprocalRank(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'CutOffs', EXPERIMENT.measure.err.cutoffs, 'FixNumberRetrievedDocuments', EXPERIMENT.measure.err.fixNumberRetrievedDocuments, 'FixedNumberRetrievedDocumentsPaddingStrategy', EXPERIMENT.measure.err.fixedNumberRetrievedDocumentsPaddingStrategy);

% Configuration for Twist
EXPERIMENT.measure.twist.id = 'twist';
EXPERIMENT.measure.twist.acronym = 'Twist';
EXPERIMENT.measure.twist.name = 'Twist';
EXPERIMENT.measure.twist.fixNumberRetrievedDocuments = 1000;
EXPERIMENT.measure.twist.fixedNumberRetrievedDocumentsPaddingStrategy = 'NotRelevant';
EXPERIMENT.measure.twist.compute = @(pool, runSet, shortNameSuffix) twist(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'FixNumberRetrievedDocuments', EXPERIMENT.measure.twist.fixNumberRetrievedDocuments, 'FixedNumberRetrievedDocumentsPaddingStrategy', EXPERIMENT.measure.twist.fixedNumberRetrievedDocumentsPaddingStrategy);

% Returns the identifier of a measure given its index in EXPERIMENT.measure.list
EXPERIMENT.measure.getID = @(idx) ( EXPERIMENT.measure.(EXPERIMENT.measure.list{idx}).id ); 

% Returns the acronym of a measure given its index in EXPERIMENT.measure.list
EXPERIMENT.measure.getAcronym = @(idx) ( EXPERIMENT.measure.(EXPERIMENT.measure.list{idx}).acronym ); 

% Returns the name of a measure given its index in EXPERIMENT.measure.list
EXPERIMENT.measure.getName = @(idx) ( EXPERIMENT.measure.(EXPERIMENT.measure.list{idx}).name ); 


%% Configuration for analyses

% The significance level for the analyses
EXPERIMENT.analysis.alpha.threshold = 0.05;
EXPERIMENT.analysis.alpha.color = 'lightgrey';

EXPERIMENT.analysis.smallEffect.threshold = 0.06;
EXPERIMENT.analysis.smallEffect.color = 'verylightblue';

EXPERIMENT.analysis.mediumEffect.threshold = 0.14;
EXPERIMENT.analysis.mediumEffect.color = 'lightblue';

EXPERIMENT.analysis.largeEffect.color = 'blue';
    
%Configuration for ANOVA factorial analyses
EXPERIMENT.analysis.rq1.id = 'rq1';
EXPERIMENT.analysis.rq1.name = 'Topic/System/Corpus Effects';
EXPERIMENT.analysis.rq1.description = 'Crossed effects GLMM: subjects are topics; systems and corpora are effects';



EXPERIMENT.analysis.rq1a.id = 'rq1a';
EXPERIMENT.analysis.rq1a.name = 'Topic/System Effects on Whole Corpus';
EXPERIMENT.analysis.rq1a.description = 'Crossed effects GLMM on whole corpus: subjects are topics; system are effects';
EXPERIMENT.analysis.rq1a.subject = 'Topic';
EXPERIMENT.analysis.rq1a.factorA = 'System';
% the model = Topic (subject) + System (factorA)
EXPERIMENT.analysis.rq1a.model = [1 0; ... 
                                  0 1];
EXPERIMENT.analysis.rq1a.compute = @(data, subject, factorA) anovan(data, {subject, factorA}, ...
        'Model', EXPERIMENT.analysis.rq1a.model, ...
        'VarNames', {EXPERIMENT.analysis.rq1a.subject, EXPERIMENT.analysis.rq1a.factorA}, ...
        'sstype', 3, 'alpha', EXPERIMENT.analysis.alpha.threshold, 'display', 'off');    

%Configuration for ANOVA factorial analyses
EXPERIMENT.analysis.rq1b.id = 'rq1b';
EXPERIMENT.analysis.rq1b.name = 'Topic/System Effects on Sub-corpora';
EXPERIMENT.analysis.rq1b.description = 'Crossed effects GLMM on sub-corpora: subjects are topics; systems  are effects';
EXPERIMENT.analysis.rq1b.subject = 'Topic';
EXPERIMENT.analysis.rq1b.factorA = 'System';
% the model = Topic (subject) + System (factorA) 
EXPERIMENT.analysis.rq1b.model = [1 0; ... 
                                  0 1];
EXPERIMENT.analysis.rq1b.compute = @(data, subject, factorA) anovan(data, {subject, factorA}, ...
        'Model', EXPERIMENT.analysis.rq1b.model, ...
        'VarNames', {EXPERIMENT.analysis.rq1b.subject, EXPERIMENT.analysis.rq1b.factorA}, ...
        'sstype', 3, 'alpha', EXPERIMENT.analysis.alpha.threshold, 'display', 'off');    
    
    
%Configuration for ANOVA factorial analyses
EXPERIMENT.analysis.rq1c.id = 'rq1c';
EXPERIMENT.analysis.rq1c.name = 'Topic/System/Corpus Effects on Sub-corpora';
EXPERIMENT.analysis.rq1c.description = 'Crossed effects GLMM on sub-corpora: subjects are topics; systems and corpora are effects';
EXPERIMENT.analysis.rq1c.subject = 'Topic';
EXPERIMENT.analysis.rq1c.factorA = 'System';
EXPERIMENT.analysis.rq1c.factorB = EXPERIMENT.corpus.name;
% the model = Topic (subject) + System (factorA) + Corpus (factorB)
EXPERIMENT.analysis.rq1c.model = [1 0 0; ... 
                                  0 1 0; ...
                                  0 0 1; ...
                                  0 1 1];
EXPERIMENT.analysis.rq1c.compute = @(data, subject, factorA, factorB) anovan(data, {subject, factorA, factorB}, ...
        'Model', EXPERIMENT.analysis.rq1c.model, ...
        'VarNames', {EXPERIMENT.analysis.rq1c.subject, EXPERIMENT.analysis.rq1c.factorA, EXPERIMENT.analysis.rq1c.factorB}, ...
        'sstype', 3, 'alpha', EXPERIMENT.analysis.alpha.threshold, 'display', 'off');
    

    


        
%% split_pool
% 
% Splits a pool into different pools, each one corresponding to one of the
% corpora of the specified split.
%
%% Synopsis
%
%   [] = split_pool(trackID, splitID)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track to process.
% * *|splitID|* - the identifier of the split to process.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2015b or higher
% * *Copyright:* (C) 2016-2017 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%
function [] = split_pool(trackID, splitID)
    
    % check that we have the correct number of input arguments. 
    narginchk(2, 2);
    
    % setup common parameters
    common_parameters;
        
    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');    
    
    % check that splitID is a non-empty string
    validateattributes(splitID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'splitID');

     if iscell(splitID)
        % check that splitID is a cell array of strings with one element
        assert(iscellstr(splitID) && numel(splitID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected splitID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    splitID = char(strtrim(splitID));
    splitID = splitID(:).';
    
    % check that splitID assumes a valid value
    validatestring(splitID, ...
        EXPERIMENT.split.list, '', 'splitID');  
    
    % check that the track and the split rely on the same corpus
    assert(strcmp(EXPERIMENT.track.(trackID).corpus, EXPERIMENT.split.(splitID).partOf), 'Track %s and split %s do not rely on the same corpus', trackID, splitID);
        
    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Splitting pool for track %s (%s) ########\n\n', EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - split: %s\n', splitID);


    fprintf('+ Loading pool of the super-corpus\n');
        
    poolID = EXPERIMENT.pattern.identifier.pool(EXPERIMENT.split.(splitID).partOf, trackID);
    fprintf('  - %s\n', poolID);
    
    serload2(EXPERIMENT.pattern.file.dataset(trackID, poolID), ...
        'WorkspaceVarNames', {'partOfPool'}, ...
        'FileVarNames', {poolID});
    
    % the total number of topics in the pool
    T = height(partOfPool);
    
    
    % create a dummy table with a dummy document and the lowest relevance
    % degree possible among those in the pool
    relevanceDegrees = categories(partOfPool{:, 1}{1, 1}.RelevanceDegree);
    
    dummyTable = cell2table({'#####DUMMY_DOC_ID^^^^^^', relevanceDegrees(1)});
    dummyTable.Properties.VariableNames = {'Document', 'RelevanceDegree'};
    
    
    fprintf('+ Processing sub-corpora\n');
    
    % for each subcorpus
    for s = 1:length(EXPERIMENT.split.(splitID).corpus)
    
        start = tic;
        
        corpusID = EXPERIMENT.split.(splitID).corpus{s};
        
        fprintf('  - subcorpus %s\n', corpusID);
        
        fprintf('    * loading sub-corpus\n');
        
        corpus = importdata(EXPERIMENT.pattern.file.corpus(corpusID));
        
        fprintf('    * computing sub-corpus pool\n');
                
        corpusPool = partOfPool;
        
        % for each topic
        for t = 1:T
            
            % extract the inner table contained in the topic
            topic = corpusPool{t, 1}{1, 1};

            % find which rows (documents) have to be kept
            keep = ismember(topic.Document, corpus);

            if any(keep)
                corpusPool{t, 1} = {topic(keep, :)};
            else
                corpusPool{t, 1} = {dummyTable};
            end;
            
        end; % for topic
        
        
        poolID = EXPERIMENT.pattern.identifier.pool(corpusID, trackID);
        corpusPool.Properties.UserData.identifier = poolID;
        corpusPool.Properties.VariableNames = {poolID};
                
        fprintf('    * saving sub-corpus pool\n');
        
        sersave2(EXPERIMENT.pattern.file.dataset(trackID, poolID), ...
            'WorkspaceVarNames', {'corpusPool'}, ...
            'FileVarNames', {poolID});
        
        clear corpus corpusPool
        
        fprintf('    * elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
        
    end; % for subcorpus
    
    fprintf('\n\n######## Total elapsed time for splitting pool for track %s (%s): %s ########\n\n', ...
            EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
    
end


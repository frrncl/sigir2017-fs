%% rq1_plot_effects
% 
% Plots the main and interaction effects for the Topic/System/Corpus 
% Effects ANOVA.

%% Synopsis
%
%   [] = rq1c_plot_effects_multcompare(trackID, splitID, startMeasure, endMeasure)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|splitID|* - the identifier of the split to process.
% * *|startMeasure|* - the index of the start measure to analyse. Optional.
% * *|endMeasure|* - the index of the end measure to analyse. Optional.
%
% *Returns*
%
% Nothing.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2015b or higher
% * *Copyright:* (C) 2016-2017 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [] = rq1c_plot_effects(trackID, splitID, startMeasure, endMeasure)

    persistent TAG;
    
    if isempty(TAG)
        TAG = 'rq1c';
    end;
    
    % check the number of input parameters
    narginchk(2, 4);

    % load common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
              
    % check that splitID is a non-empty cell array
    validateattributes(splitID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'splitID');
    
    if iscell(splitID)
        % check that splitID is a cell array of strings with one element
        assert(iscellstr(splitID) && numel(splitID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected splitID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    splitID = char(strtrim(splitID));
    splitID = splitID(:).';
    
    % check that splitID assumes a valid value
    validatestring(splitID, ...
        EXPERIMENT.split.list, '', 'splitID');
   
    if nargin == 4
        validateattributes(startMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number }, '', 'startMeasure');
        
        validateattributes(endMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startMeasure, '<=', EXPERIMENT.measure.number }, '', 'endMeasure');
        
    else
        startMeasure = 1;
        endMeasure = EXPERIMENT.measure.number;
    end;  
        
    % start of overall computations
    startComputation = tic;
            
    fprintf('\n\n######## Plotting %s main and interaction effects on track %s (%s) ########\n\n', ...
        TAG, EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - analysis type:\n');
    fprintf('    * %s\n', TAG);
    fprintf('    * %s\n', EXPERIMENT.analysis.(TAG).name);
    fprintf('    * %s\n', EXPERIMENT.analysis.(TAG).description);
    fprintf('  - track %s\n', trackID);
    fprintf('  - split: %s\n', splitID);    fprintf('  - measures:\n');
    fprintf('    * start measure: %d (%s)\n', startMeasure, EXPERIMENT.measure.getAcronym(startMeasure));
    fprintf('    * end measure: %d (%s)\n', endMeasure, EXPERIMENT.measure.getAcronym(endMeasure));
    fprintf('  - significance level alpha: %f\n', EXPERIMENT.analysis.alpha.threshold);
    
     % for each measure
    for m = startMeasure:endMeasure
        
        fprintf('\n+ Plotting effects and multcompare of of %s\n', EXPERIMENT.measure.getAcronym(m));
        
        mid = EXPERIMENT.measure.list{m};
           
        anovaID = EXPERIMENT.pattern.identifier.anova.analysis(TAG, mid, splitID, trackID);
        anovaMeID = EXPERIMENT.pattern.identifier.anova.me(TAG, mid, splitID, trackID);
        anovaIeID = EXPERIMENT.pattern.identifier.anova.ie(TAG, mid, splitID, trackID);
        anovaStatsID = EXPERIMENT.pattern.identifier.anova.sts(TAG, mid, splitID, trackID);
        serload2(EXPERIMENT.pattern.file.analysis(trackID, anovaID), ...
                'WorkspaceVarNames', {'me', 'ie', 'sts'}, ...
                'FileVarNames', {anovaMeID, anovaIeID, anovaStatsID});
            
        
        yMin = Inf;
        yMax = -Inf;

            
        currentFigure = figure('Visible', 'off');

        subplot(1, 2, 1);

            xTick = 1:length(me.corpus);
            data.mean = me.factorB.mean.';
            data.ciLow = me.factorB.ci(:, 1).';
            data.ciHigh = me.factorB.ci(:, 2).';        

            plot(xTick, data.mean, 'Color', rgb('ForestGreen'), 'LineWidth', 3);

            hold on

            hFill = fill([xTick fliplr(xTick)],[data.ciHigh fliplr(data.ciLow)], rgb('ForestGreen'), ...
                        'LineStyle', 'none', 'EdgeAlpha', 0.10, 'FaceAlpha', 0.10);

            % send the fill to back
            uistack(hFill, 'bottom');

            % Exclude fill from legend
            set(get(get(hFill, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle','off');

            ax1 = gca;
            ax1.TickLabelInterpreter = 'latex';
            ax1.FontSize = 24;

            ax1.XTick = xTick;
            ax1.XTickLabel = cellfun(@(x) sprintf('\\texttt{%s}', x), me.corpus, 'UniformOutput', false);
            ax1.TickLabelInterpreter = 'latex';
            %ax1.XTickLabelRotation = 90;

            ax1.XLabel.Interpreter = 'latex';
            ax1.XLabel.String = 'Sub-corpus';

            ax1.YLabel.Interpreter = 'latex';
            ax1.YLabel.String = sprintf('%s Marginal Mean', EXPERIMENT.measure.(mid).acronym);

            title(sprintf('%s Main Effects', 'Sub-corpus'), 'FontSize', 24, 'Interpreter', 'latex');            
            
            yMin = min(yMin, ax1.YLim(1));
            yMax = max(yMax, ax1.YLim(2));     
        
        
       subplot(1, 2, 2);     
       
           data.mean = ie.factorBA.mean.';

            hold on;

            x = 1:length(me.corpus);
            
            colors = parula(length(me.system));

            % for each system
            for l = 1:length(me.system)        
                plot(x, data.mean(l, :), 'Color', colors(l, :), 'LineWidth', 1.5);
            end;

            ax3 = gca;
            ax3.TickLabelInterpreter = 'latex';
            ax3.FontSize = 24;

            ax3.XTick = x;
            ax3.XTickLabel = cellfun(@(x) sprintf('\\texttt{%s}', x), ie.corpus, 'UniformOutput', false);
            ax3.TickLabelInterpreter = 'latex';
            %ax3.XTickLabelRotation = 90;

            ax3.XLabel.Interpreter = 'latex';
            ax3.XLabel.String = 'Sub-corpus';

            ax3.YLabel.Interpreter = 'latex';
            ax3.YLabel.String = sprintf('%s Marginal Mean', EXPERIMENT.measure.(mid).acronym);

            title(sprintf('%s*%s Interaction Effects', 'Sub-corpus', 'System'), 'FontSize', 24, 'Interpreter', 'latex');

            %legend(EXPERIMENT.topics.list, 'Location', 'BestOutside', 'Interpreter', 'tex')
            
%            yMin = min(yMin, ax3.YLim(1));
%            yMax = max(yMax, ax3.YLim(2));
       
       
        ax1.YLim = [yMin yMax];

        ax2.XLim = [yMin yMax];
        
%        ax3.YLim = [yMin yMax];

        currentFigure.PaperPositionMode = 'auto';
        currentFigure.PaperUnits = 'centimeters';
        currentFigure.PaperSize = [62 22];
        currentFigure.PaperPosition = [1 1 60 20];

        figureID = EXPERIMENT.pattern.identifier.anova.mie(TAG, mid, splitID, trackID);

        print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(trackID, figureID));

        close(currentFigure)
    
    end; % measure
    
       
    fprintf('\n\n######## Total elapsed time for plotting %s main and interaction effects on track %s (%s): %s ########\n\n', ...
           TAG, ...
           EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
       
end



function [] =  highlightTopGroup(h, m, usemax, cmpNum, color)

    common_parameters;
    
    % the index of the top element in the top group
    if usemax
        [~, topElement] = max(m(:, 1));
    else
        [~, topElement] = min(m(:, 1));
    end;
    
    % Get bounds for the top group
    topGroupHandle = findobj(h, 'UserData', -topElement);
    if (isempty(topGroupHandle))
        return; 
    end % unexpected

    topGroupX = get(topGroupHandle, 'XData');
    topGroupLower = min(topGroupX);
    topGroupUpper = max(topGroupX);
    
    % Change the comparison lines to use these values
    comparisonLines = findobj(h, 'UserData', 'Comparison lines');
    if (~isempty(comparisonLines))
       comparisonLines.LineWidth = 2;
       comparisonLines.XData(1:2) = topGroupLower;
       comparisonLines.XData(4:5) = topGroupUpper;
    end

    % arrange line styles and markers
    for e=1:cmpNum

        % look for the line and marker of the current element
        lineHandle = findobj(h, 'UserData', -e, 'Type','Line', '-and', 'LineStyle', '-', '-and', 'Marker', 'none');
        markerHandle = findobj(h, 'UserData', e, 'Type','Line', '-and', 'LineStyle', 'none', '-and', 'Marker', 'o');

        if (isempty(lineHandle))
            continue;
        end  % unexpected

        currentElementX = get(lineHandle, 'XData');
        currentElementLower = min(currentElementX);
        currentElementUpper = max(currentElementX);
        
        % To be in the top group the upper bound of the current element
        % (CEL) must be above the lower bound of the top group (TGL) and
        % the lower bound of the current element (CEL) must be below the
        % upper bound of the top groud (TGL)
        %
        %  CEL    TGL   CEU  TGU
        %   |      |_____|____|
        %   |____________|
        %
        %  TGL   CEL  TGU     CEU
        %   |_____|____|       |
        %         |____________|
        %
        if ( currentElementUpper > topGroupLower && currentElementLower < topGroupUpper)
            lineHandle.Color = color;
            lineHandle.LineWidth = 2.5;
            lineHandle.Visible = 'on';
            markerHandle.MarkerFaceColor = color;
            markerHandle.MarkerEdgeColor = color;
            markerHandle.MarkerSize = 10;
        else
            lineHandle.Color = 'k';
            lineHandle.LineWidth = 1.5;
            lineHandle.Visible = 'on';
            markerHandle.MarkerFaceColor = 'k';
            markerHandle.MarkerEdgeColor = 'k';
            markerHandle.MarkerSize = 6;
        end;
        
    end; % for tags
end



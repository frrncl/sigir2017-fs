%% rq1c_analysis
% 
% Computes the Topic/System/Corpus Effects ANOVA on the sub-corpora.

%% Synopsis
%
%   [] = rq1c_analysis(trackID, splitID, startMeasure, endMeasure)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|splitID|* - the identifier of the split to process.
% * *|startMeasure|* - the index of the start measure to analyse. Optional.
% * *|endMeasure|* - the index of the end measure to analyse. Optional.
%
%
% *Returns*
%
% Nothing.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2015b or higher
% * *Copyright:* (C) 2016-2017 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [] = rq1c_analysis(trackID, splitID, startMeasure, endMeasure)

    persistent TAG;
    
    if isempty(TAG)
        TAG = 'rq1c';      
    end;

    % check the number of input parameters
    narginchk(2, 4);

    % load common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
        
    % check that splitID is a non-empty cell array
    validateattributes(splitID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'splitID');
    
    if iscell(splitID)
        % check that splitID is a cell array of strings with one element
        assert(iscellstr(splitID) && numel(splitID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected splitID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    splitID = char(strtrim(splitID));
    splitID = splitID(:).';
    
    % check that splitID assumes a valid value
    validatestring(splitID, ...
        EXPERIMENT.split.list, '', 'splitID');
    
    % check that the track and the split rely on the same corpus
    assert(strcmp(EXPERIMENT.track.(trackID).corpus, EXPERIMENT.split.(splitID).partOf), 'Track %s and split %s do not rely on the same corpus', trackID, splitID);
        
    if nargin == 4
        validateattributes(startMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number }, '', 'startMeasure');
        
        validateattributes(endMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startMeasure, '<=', EXPERIMENT.measure.number }, '', 'endMeasure');
        
    else
        startMeasure = 1;
        endMeasure = EXPERIMENT.measure.number;
    end;
    
    % start of overall computations
    startComputation = tic;
                  
    fprintf('\n\n######## Performing %s ANOVA analysis on track %s (%s) ########\n\n', ...
        TAG, EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - analysis type:\n');
    fprintf('    * %s\n', TAG);
    fprintf('    * %s\n', EXPERIMENT.analysis.(TAG).name);
    fprintf('    * %s\n', EXPERIMENT.analysis.(TAG).description);
    fprintf('  - track %s\n', trackID);
    fprintf('  - split: %s\n', splitID);
    fprintf('  - measures:\n');
    fprintf('    * start measure: %d (%s)\n', startMeasure, EXPERIMENT.measure.getAcronym(startMeasure));
    fprintf('    * end measure: %d (%s)\n', endMeasure, EXPERIMENT.measure.getAcronym(endMeasure));
    fprintf('  - significance level alpha: %f\n', EXPERIMENT.analysis.alpha.threshold);
            
              
    % for each measure
    for m = startMeasure:endMeasure
         
        start = tic;
        
        fprintf('\n+ Analysing %s\n', EXPERIMENT.measure.getAcronym(m));
        
        fprintf('  - loading the data\n');
        
        mid = EXPERIMENT.measure.list{m};
        
        C = length(EXPERIMENT.split.(splitID).corpus);
                
        measures = cell(1, C);
        
        % for each sub-corpus
        for c = 1:C
            
            corpusID = EXPERIMENT.split.(splitID).corpus{c};
            
            measureID = EXPERIMENT.pattern.identifier.measure(mid, corpusID, trackID);
            
            serload2(EXPERIMENT.pattern.file.measure(trackID, measureID), ...
                'WorkspaceVarNames', {'measure'}, ...
                'FileVarNames', {measureID});
            
            measures{c} = measure;
            
            if m == 1
                % determine the topics for which there are no more relevant
                % documents
                topicsToSkip(:, c) = sum(isnan(measure{:, :}), 2) > 0;
                
                fprintf('    * sub-corpus %s: total topics %d; topics without relevant documents %d\n', ...
                    corpusID, length(topicsToSkip(:, c)), sum(topicsToSkip(:, c)) );
            end;
            
            clear measure;
            
        end; % for sub-corpus
        
        if m == 1
            fprintf('    * total number of topics without relevant documents across all the sub-corpora %d\n', ...
                sum(sum(topicsToSkip, 2) > 0));
            
            topicsToKeep = sum(topicsToSkip, 2) > 0;            
            topicsToKeep = ~topicsToKeep;
        end;
        
        % load the whole corpus measure
        measureID = EXPERIMENT.pattern.identifier.measure(mid, EXPERIMENT.split.(splitID).partOf, trackID);
        
        
        serload2(EXPERIMENT.pattern.file.measure(trackID, measureID), ...
            'WorkspaceVarNames', {'measure'}, ...
            'FileVarNames', {measureID});
        
        % order systems by descending mean of the measure on the whole
        % corpus after removing the topics without relevant documents
        [~, idx] = sort(mean(measure{topicsToKeep, :}), 'descend');
        
        clear measure;
                
        % for each sub-corpus, remove the topics without relevant
        % documents across all the sub-corpora
        for c = 1:C
            measures{c} = measures{c}(topicsToKeep, idx);
        end;
        
        % the number of topics and runs
        T = height(measures{1});
        R = width(measures{1});
        
        % total number of elements in the list
        N = T * R * C;
        
        % the data is layed out as follows:
        %
        % Data      Subject         FactorA FactorB
        % 0.10      T1              S1      C1
        % 0.20      T2              S1      C1
        % 0.30      T1              S2      C1
        % 0.40      T2              S2      C1
        % 0.50      T1              S3      C1
        % 0.60      T2              S3      C1
        % 0.15      T1              S1      C2
        % 0.25      T2              S1      C2
        % 0.35      T1              S2      C2
        % 0.45      T2              S2      C2
        % 0.55      T1              S3      C2
        % 0.65      T2              S3      C2
        
        data = NaN(1, N);
        
        % grouping variable for the subjects (topic)
        subject = repmat(measures{1}.Properties.RowNames, C*R, 1);
        
        % grouping variable for factorA (system)
        factorA = repmat(measures{1}.Properties.VariableNames, T, 1);
        factorA = repmat(factorA(:), C, 1);
        
        % grouping variable for factorB (sub-corpus)
        factorB = repmat(EXPERIMENT.split.(splitID).corpus, T*R, 1);
        factorB = factorB(:);
        
        % for each sub-corpus
        for c = 1:C
            % copy the measures in the correct range of the data
            data((c-1)*T*R + 1 : c*T*R) = measures{c}{:, :}(:);
        end;
        
        clear measures;
        
        fprintf('  - analysing the data\n');
                
        [~, tbl, sts] = EXPERIMENT.analysis.(TAG).compute(data, subject, ...
            factorA, factorB);
        
        df_topic = tbl{2,3};
        ss_topic = tbl{2,2};
        F_topic = tbl{2,6};
        
        df_system = tbl{3,3};
        ss_system = tbl{3,2};
        F_system = tbl{3,6};
        
        df_corpus = tbl{4,3};
        ss_corpus = tbl{4,2};
        F_corpus = tbl{4,6};
        
        df_system_corpus = tbl{5,3};
        ss_system_corpus = tbl{5,2};
        F_system_corpus = tbl{5,6};
        
        ss_error = tbl{6, 2};
        df_error = tbl{6, 3};
        
        ss_total = tbl{7, 2};
        
        % compute the strength of association        
        soa.omega2p.topic = df_topic * (F_topic - 1) / (df_topic * (F_topic - 1) + N);
        soa.omega2p.system = df_system * (F_system - 1) / (df_system * (F_system - 1) + N);
        soa.omega2p.corpus =  df_corpus * (F_corpus - 1) / (df_corpus * (F_corpus - 1) + N);
        soa.omega2p.system_corpus = df_system_corpus * (F_system_corpus - 1) / (df_system_corpus * (F_system_corpus - 1) + N);
        
        soa.eta2.topic = ss_topic / ss_total;
        soa.eta2.system = ss_system / ss_total;
        soa.eta2.corpus = ss_corpus / ss_total;
        soa.eta2.system_corpus = ss_system_corpus / ss_total;
        
        soa.eta2p.topic = ss_topic / (ss_topic + ss_error);
        soa.eta2p.system = ss_system / (ss_system + ss_error);
        soa.eta2p.corpus = ss_corpus / (ss_corpus + ss_error);
        soa.eta2p.system_corpus = ss_system_corpus / (ss_system_corpus + ss_error);

        % raw observations
        obs.data = data;
        obs.subject = subject;
        obs.factorA = factorA;
        obs.factorB = factorB;
        
        % main effects
        me.system = unique(factorA, 'stable');
        me.corpus = unique(factorB, 'stable');
        [me.factorA.mean, me.factorA.ci] = grpstats(data(:), factorA(:), {'mean', 'meanci'});
        [me.factorB.mean, me.factorB.ci] = grpstats(data(:), factorB(:), {'mean', 'meanci'});
                
        % interaction between corpora (x-axis) and system (y-axis)
        % each row is a corpus, columns are systems        
        ie.system = me.system;
        ie.corpus = me.corpus;
        ie.factorBA.mean = grpstats(data(:), {factorB(:),factorA(:)}, {'mean'});
        ie.factorBA.mean = reshape(ie.factorBA.mean, R, C).';
        
        anovaID = EXPERIMENT.pattern.identifier.anova.analysis(TAG, mid, splitID, trackID);
        anovaObsID = EXPERIMENT.pattern.identifier.anova.obs(TAG, mid, splitID, trackID);
        anovaMeID = EXPERIMENT.pattern.identifier.anova.me(TAG, mid, splitID, trackID);
        anovaIeID = EXPERIMENT.pattern.identifier.anova.ie(TAG, mid, splitID, trackID);
        anovaTableID = EXPERIMENT.pattern.identifier.anova.tbl(TAG, mid, splitID, trackID);
        anovaStatsID = EXPERIMENT.pattern.identifier.anova.sts(TAG, mid, splitID, trackID);
        anovaSoAID = EXPERIMENT.pattern.identifier.anova.soa(TAG, mid, splitID, trackID);
        
        sersave2(EXPERIMENT.pattern.file.analysis(trackID, anovaID), ...
                'WorkspaceVarNames', {'obs', 'me', 'ie', 'tbl', 'sts', 'soa'}, ...
                'FileVarNames', {anovaObsID, anovaMeID, anovaIeID, anovaTableID, anovaStatsID, anovaSoAID});
        
        
        clear data obs me ie tbl sts soa;
        
        fprintf('  - elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
                     
    end; % measure
        
           
    fprintf('\n\n######## Total elapsed time for performing %s ANOVA analysis on track %s (%s): %s ########\n\n', ...
           TAG, ...
           EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
       
    diary off;
end


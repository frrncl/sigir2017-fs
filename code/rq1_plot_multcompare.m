%% rq1_plot_multcompare
% 
% Plots the Tukey HSD multiple comparisons for the different RQ1 analyses.

%% Synopsis
%
%   [] = rq1_plot_multcompare(trackID, splitID, startMeasure, endMeasure)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|splitID|* - the identifier of the split to process.
% * *|startMeasure|* - the index of the start measure to analyse. Optional.
% * *|endMeasure|* - the index of the end measure to analyse. Optional.
%
% *Returns*
%
% Nothing.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2015b or higher
% * *Copyright:* (C) 2016-2017 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [] = rq1_plot_multcompare(trackID, splitID, startMeasure, endMeasure)

    persistent TAG TAGa TAGb TAGc;
    
    if isempty(TAG)
        TAG = 'rq1';
        TAGa = 'rq1a';  % topic/system on whole corpus
        TAGb = 'rq1b';  % topic/system on sub-corpora
        TAGc = 'rq1c';  % topic/system/corpus on sub-corpora
    end;
    
    % check the number of input parameters
    narginchk(2, 4);

    % load common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
              
    % check that splitID is a non-empty cell array
    validateattributes(splitID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'splitID');
    
    if iscell(splitID)
        % check that splitID is a cell array of strings with one element
        assert(iscellstr(splitID) && numel(splitID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected splitID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    splitID = char(strtrim(splitID));
    splitID = splitID(:).';
    
    % check that splitID assumes a valid value
    validatestring(splitID, ...
        EXPERIMENT.split.list, '', 'splitID');
       
    if nargin == 4
        validateattributes(startMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number }, '', 'startMeasure');
        
        validateattributes(endMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startMeasure, '<=', EXPERIMENT.measure.number }, '', 'endMeasure');
        
    else
        startMeasure = 1;
        endMeasure = EXPERIMENT.measure.number;
    end;     
     
    % start of overall computations
    startComputation = tic;
            
    fprintf('\n\n######## Plotting %s Tukey HSD multiple comparisons on track %s (%s) ########\n\n', ...
        TAG, EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - analysis type:\n');
    fprintf('    * %s\n', TAG);
    fprintf('    * %s\n', EXPERIMENT.analysis.(TAG).name);
    fprintf('    * %s\n', EXPERIMENT.analysis.(TAG).description);
    fprintf('  - track %s\n', trackID);
    fprintf('  - split: %s\n', splitID);    fprintf('  - measures:\n');    
    fprintf('  - measures:\n');
    fprintf('    * start measure: %d (%s)\n', startMeasure, EXPERIMENT.measure.getAcronym(startMeasure));
    fprintf('    * end measure: %d (%s)\n', endMeasure, EXPERIMENT.measure.getAcronym(endMeasure));
    fprintf('  - significance level alpha: %f\n', EXPERIMENT.analysis.alpha.threshold);
    
    % for each measure
    for m = startMeasure:endMeasure
        
        fprintf('\n+ Plotting Tukey HSD of %s\n', EXPERIMENT.measure.getAcronym(m));
        
        mid = EXPERIMENT.measure.list{m};
        
        % topic/system on whole corpus
        anovaID = EXPERIMENT.pattern.identifier.anova.analysis(TAGa, mid, splitID, trackID); 
        anovaMeID = EXPERIMENT.pattern.identifier.anova.me(TAGa, mid, splitID, trackID);
        anovaStatsID = EXPERIMENT.pattern.identifier.anova.sts(TAGa, mid, splitID, trackID);
        serload2(EXPERIMENT.pattern.file.analysis(trackID, anovaID), ...
                'WorkspaceVarNames', {'stsA', 'meA'}, ...
                'FileVarNames', {anovaStatsID, anovaMeID});
        
        % topic/system on sub-corpora
        anovaID = EXPERIMENT.pattern.identifier.anova.analysis(TAGb, mid, splitID, trackID);    
        anovaMeID = EXPERIMENT.pattern.identifier.anova.me(TAGb, mid, splitID, trackID);
        anovaStatsID = EXPERIMENT.pattern.identifier.anova.sts(TAGb, mid, splitID, trackID);
        serload2(EXPERIMENT.pattern.file.analysis(trackID, anovaID), ...
                'WorkspaceVarNames', {'stsB', 'meB'}, ...
                'FileVarNames', {anovaStatsID, anovaMeID});
            
        % topic/system/corpus on sub-corpora
        anovaID = EXPERIMENT.pattern.identifier.anova.analysis(TAGc, mid, splitID, trackID);
        anovaMeID = EXPERIMENT.pattern.identifier.anova.me(TAGc, mid, splitID, trackID);
        anovaStatsID = EXPERIMENT.pattern.identifier.anova.sts(TAGc, mid, splitID, trackID);
        serload2(EXPERIMENT.pattern.file.analysis(trackID, anovaID), ...
                'WorkspaceVarNames', {'stsC', 'meC'}, ...
                'FileVarNames', {anovaStatsID, anovaMeID});
                           

        %--------------------------------------------------------------
        % topic/system on whole corpus
        mcFigureHandles{1} = figure('Visible', 'off');
       
        % comparing systems
        [c, mm, ~, ~] = multcompare(stsA, 'ctype', 'hsd', 'alpha', EXPERIMENT.analysis.alpha.threshold, 'dimension', [2]);
                
        fprintf('  - topic/system on whole corpus\n');
        
        % total number of significantly different pairs
        tmp = sum(c(:, 6) < EXPERIMENT.analysis.alpha.threshold);
        
        fprintf('    * total number of significantly different pairs: %d out of %d (%5.2f%%)\n', ...
            tmp, size(c, 1), tmp/size(c, 1) * 100);
        
        % the index of the top element
        teidx = find(meA.factorA.mean == max(meA.factorA.mean));
        
        % number of significantly different pairs for the top group 
        tmp = sum(c(c(:, 1) == teidx | c(:, 2) == teidx, 6) < EXPERIMENT.analysis.alpha.threshold);
        
        fprintf('    * number of significantly different elements in the top group: %d out of %d (%5.2f%%)\n', ...
            tmp, length(meA.factorA.mean), tmp/length(meA.factorA.mean) * 100);
        
        ax = gca;
        mcAxesHandles{1} = ax;
        ax.FontSize = 24;

        ax.Title.String = '$Y_{ij} = \mu_{\cdot\cdot} + \tau_i + \alpha_j$ on whole corpus';
        ax.Title.FontSize = 24;
        ax.Title.Interpreter = 'latex';

        ax.TickLabelInterpreter = 'latex';
        
        ax.XLabel.String = sprintf('%s Marginal Mean', EXPERIMENT.measure.(mid).acronym);
        ax.XLabel.Interpreter = 'latex';

        ax.YLabel.String = 'System -- $\alpha_j$';
        ax.YLabel.Interpreter = 'latex';

        ax.YTickLabel = [];

        highlightTopGroup(ax, mm, true, length(meA.system), rgb('RoyalBlue'));
                        
        mcLimits.lower(1) = ax.XLim(1);
        mcLimits.upper(1) = ax.XLim(2);        
                    
            
            
        %--------------------------------------------------------------
        % topic/system on sub-corpora
        mcFigureHandles{2} = figure('Visible', 'off');
       
        % comparing systems
        [c, mm, ~, ~] = multcompare(stsB, 'ctype', 'hsd', 'alpha', EXPERIMENT.analysis.alpha.threshold, 'dimension', [2]);
        
        fprintf('  - topic/system on sub-corpora\n');
        
        % total number of significantly different pairs
        tmp = sum(c(:, 6) < EXPERIMENT.analysis.alpha.threshold);
        
        fprintf('    * total number of significantly different pairs: %d out of %d (%5.2f%%)\n', ...
            tmp, size(c, 1), tmp/size(c, 1) * 100);
        
        % the index of the top element
        teidx = find(meB.factorA.mean == max(meB.factorA.mean));
        
        % number of significantly different pairs for the top group 
        tmp = sum(c(c(:, 1) == teidx | c(:, 2) == teidx, 6) < EXPERIMENT.analysis.alpha.threshold);
        
        fprintf('    * number of significantly different elements in the top group: %d out of %d (%5.2f%%)\n', ...
            tmp, length(meB.factorA.mean), tmp/length(meB.factorA.mean) * 100);
        
        ax = gca;
        mcAxesHandles{2} = ax;
        ax.FontSize = 24;

        ax.Title.String = '$Y_{ij} = \mu_{\cdot\cdot} + \tau_i + \alpha_j$ on sub-corpora';
        ax.Title.FontSize = 24;
        ax.Title.Interpreter = 'latex';

        ax.TickLabelInterpreter = 'latex';
        
        ax.XLabel.String = sprintf('%s Marginal Mean', EXPERIMENT.measure.(mid).acronym);
        ax.XLabel.Interpreter = 'latex';

        ax.YLabel.String = 'System -- $\alpha_j$';
        ax.YLabel.Interpreter = 'latex';

        ax.YTickLabel = [];

        highlightTopGroup(ax, mm, true, length(meB.system), rgb('RoyalBlue'));
                        
        mcLimits.lower(2) = ax.XLim(1);
        mcLimits.upper(2) = ax.XLim(2);        
            
        %--------------------------------------------------------------
        % topic/system/corpus on sub-corpora
        mcFigureHandles{3} = figure('Visible', 'off');
       
        % comparing systems
        [c, mm, ~, ~] = multcompare(stsC, 'ctype', 'hsd', 'alpha', EXPERIMENT.analysis.alpha.threshold, 'dimension', [2]);
        
        fprintf('  - topic/system/corpus on sub-corpora\n');
        
        % total number of significantly different pairs
        tmp = sum(c(:, 6) < EXPERIMENT.analysis.alpha.threshold);
        
        fprintf('    * total number of significantly different pairs: %d out of %d (%5.2f%%)\n', ...
            tmp, size(c, 1), tmp/size(c, 1) * 100);
        
        % the index of the top element
        teidx = find(meC.factorA.mean == max(meC.factorA.mean));
        
        % number of significantly different pairs for the top group 
        tmp = sum(c(c(:, 1) == teidx | c(:, 2) == teidx, 6) < EXPERIMENT.analysis.alpha.threshold);
        
        fprintf('    * number of significantly different elements in the top group: %d out of %d (%5.2f%%)\n', ...
            tmp, length(meC.factorA.mean), tmp/length(meC.factorA.mean) * 100);
        
        ax = gca;
        mcAxesHandles{3} = ax;
        ax.FontSize = 24;

        ax.Title.String = '$Y_{ijk} = \mu_{\cdot\cdot\cdot} + \tau_i + \alpha_j + \beta_k + (\alpha\beta)_{jk}$ on sub-corpora';
        ax.Title.FontSize = 24;
        ax.Title.Interpreter = 'latex';

        ax.TickLabelInterpreter = 'latex';
        
        ax.XLabel.String = sprintf('%s Marginal Mean', EXPERIMENT.measure.(mid).acronym);
        ax.XLabel.Interpreter = 'latex';

        ax.YLabel.String = 'System -- $\alpha_j$';
        ax.YLabel.Interpreter = 'latex';

        ax.YTickLabel = [];

        highlightTopGroup(ax, mm, true, length(meC.system), rgb('RoyalBlue'));
                        
        mcLimits.lower(3) = ax.XLim(1);
        mcLimits.upper(3) = ax.XLim(2);        
            
            
        xLim = [min(mcLimits.lower) max(mcLimits.upper)];
            
        %--------------------------------------------------------------
        % copy all the separate figures to the same plot into subplots
        currentFigure = figure('Visible', 'off');

        for k = 1:length(mcFigureHandles)
            
            % copy the plot generated by multcompare into current
            % figure
            ax = copyobj(mcAxesHandles{k}, currentFigure);
            ax.XLim = xLim;
            ax.FontSize = 24;
            
            % move it to the appropriate subplot
            subplot(1, length(mcFigureHandles), k, ax);
            
            % close the plot generated by multcompare
            close(mcFigureHandles{k});
            
        end;
        
        currentFigure.PaperPositionMode = 'auto';
        currentFigure.PaperUnits = 'centimeters';
        currentFigure.PaperSize = [72 42];
        currentFigure.PaperPosition = [1 1 70 40];

        figureID = EXPERIMENT.pattern.identifier.anova.mc(TAG, mid, splitID, trackID);

        print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(trackID, figureID));

        close(currentFigure)

    end; % for measure
    
       
    fprintf('\n\n######## Total elapsed time for plotting Tukey HSD %s multiple comparisons on track %s (%s): %s ########\n\n', ...
           TAG, ...
           EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
       
    diary off;
end

function [] =  highlightTopGroup(h, m, usemax, cmpNum, color)

    common_parameters;
    
    % number of elements in the top group
    topGroup = 0;
    
    % the index of the top element in the top group
    if usemax
        [~, topElement] = max(m(:, 1));
    else
        [~, topElement] = min(m(:, 1));
    end;
    
    % Get bounds for the top group
    topGroupHandle = findobj(h, 'UserData', -topElement);
    if (isempty(topGroupHandle))
        return; 
    end % unexpected

    topGroupX = get(topGroupHandle, 'XData');
    topGroupLower = min(topGroupX);
    topGroupUpper = max(topGroupX);
    
    % Change the comparison lines to use these values
    comparisonLines = findobj(h, 'UserData', 'Comparison lines');
    if (~isempty(comparisonLines))
       comparisonLines.LineWidth = 2;
       comparisonLines.XData(1:2) = topGroupLower;
       comparisonLines.XData(4:5) = topGroupUpper;
    end

    % arrange line styles and markers
    for e=1:cmpNum

        % look for the line and marker of the current element
        lineHandle = findobj(h, 'UserData', -e, 'Type','Line', '-and', 'LineStyle', '-', '-and', 'Marker', 'none');
        markerHandle = findobj(h, 'UserData', e, 'Type','Line', '-and', 'LineStyle', 'none', '-and', 'Marker', 'o');

        if (isempty(lineHandle))
            continue;
        end  % unexpected

        currentElementX = get(lineHandle, 'XData');
        currentElementLower = min(currentElementX);
        currentElementUpper = max(currentElementX);
        
        % To be in the top group the upper bound of the current element
        % (CEL) must be above the lower bound of the top group (TGL) and
        % the lower bound of the current element (CEL) must be below the
        % upper bound of the top groud (TGL)
        %
        %  CEL    TGL   CEU  TGU
        %   |      |_____|____|
        %   |____________|
        %
        %  TGL   CEL  TGU     CEU
        %   |_____|____|       |
        %         |____________|
        %
        if ( currentElementUpper > topGroupLower && currentElementLower < topGroupUpper)
            lineHandle.Color = color;
            lineHandle.LineWidth = 2.5;
            lineHandle.Visible = 'on';
            markerHandle.MarkerFaceColor = color;
            markerHandle.MarkerEdgeColor = color;
            markerHandle.MarkerSize = 10;
            topGroup = topGroup + 1;
        else
            lineHandle.Color = 'k';
            lineHandle.LineWidth = 1.5;
            lineHandle.Visible = 'on';
            markerHandle.MarkerFaceColor = 'k';
            markerHandle.MarkerEdgeColor = 'k';
            markerHandle.MarkerSize = 6;
        end;
        
    end; % for tags

    %fprintf('    * number of elements in the top group: %d out of %d\n', topGroup, cmpNum);
 
end










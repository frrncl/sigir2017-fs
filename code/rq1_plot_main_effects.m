%% rq1_plot_main_effects
% 
% Plots the main effects for the different RQ1 analyses.

%% Synopsis
%
%   [] = rq1_plot_main_effects(trackID, splitID, startMeasure, endMeasure)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|splitID|* - the identifier of the split to process.
% * *|startMeasure|* - the index of the start measure to analyse. Optional.
% * *|endMeasure|* - the index of the end measure to analyse. Optional.
%
% *Returns*
%
% Nothing.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2015b or higher
% * *Copyright:* (C) 2016-2017 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [] = rq1_plot_main_effects(trackID, splitID, startMeasure, endMeasure)

    persistent TAG TAGa TAGb TAGc;
    
    if isempty(TAG)
        TAG = 'rq1';
        TAGa = 'rq1a';  % topic/system on whole corpus
        TAGb = 'rq1b';  % topic/system on sub-corpora
        TAGc = 'rq1c';  % topic/system/corpus on sub-corpora
    end;
    
    % check the number of input parameters
    narginchk(2, 4);

    % load common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
              
    % check that splitID is a non-empty cell array
    validateattributes(splitID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'splitID');
    
    if iscell(splitID)
        % check that splitID is a cell array of strings with one element
        assert(iscellstr(splitID) && numel(splitID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected splitID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    splitID = char(strtrim(splitID));
    splitID = splitID(:).';
    
    % check that splitID assumes a valid value
    validatestring(splitID, ...
        EXPERIMENT.split.list, '', 'splitID');
   
    if nargin == 4
        validateattributes(startMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number }, '', 'startMeasure');
        
        validateattributes(endMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startMeasure, '<=', EXPERIMENT.measure.number }, '', 'endMeasure');
        
    else
        startMeasure = 1;
        endMeasure = EXPERIMENT.measure.number;
    end;  
        
    % start of overall computations
    startComputation = tic;
            
    fprintf('\n\n######## Plotting %s main and interaction effects on track %s (%s) ########\n\n', ...
        TAG, EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - analysis type:\n');
    fprintf('    * %s\n', TAG);
    fprintf('    * %s\n', EXPERIMENT.analysis.(TAG).name);
    fprintf('    * %s\n', EXPERIMENT.analysis.(TAG).description);
    fprintf('  - track %s\n', trackID);
    fprintf('  - split: %s\n', splitID);    fprintf('  - measures:\n');
    fprintf('    * start measure: %d (%s)\n', startMeasure, EXPERIMENT.measure.getAcronym(startMeasure));
    fprintf('    * end measure: %d (%s)\n', endMeasure, EXPERIMENT.measure.getAcronym(endMeasure));
    fprintf('  - significance level alpha: %f\n', EXPERIMENT.analysis.alpha.threshold);
    
     % for each measure
    for m = startMeasure:endMeasure
        
        fprintf('\n+ Plotting effects of %s\n', EXPERIMENT.measure.getAcronym(m));
        
        mid = EXPERIMENT.measure.list{m};
                
        % topic/system on whole corpus
        anovaID = EXPERIMENT.pattern.identifier.anova.analysis(TAGa, mid, splitID, trackID);    
        anovaMeID = EXPERIMENT.pattern.identifier.anova.me(TAGa, mid, splitID, trackID);
        serload2(EXPERIMENT.pattern.file.analysis(trackID, anovaID), ...
                'WorkspaceVarNames', {'meA'}, ...
                'FileVarNames', {anovaMeID});
            
        % topic/system on sub-corpora
        anovaID = EXPERIMENT.pattern.identifier.anova.analysis(TAGb, mid, splitID, trackID);
        anovaMeID = EXPERIMENT.pattern.identifier.anova.me(TAGb, mid, splitID, trackID);
        serload2(EXPERIMENT.pattern.file.analysis(trackID, anovaID), ...
                'WorkspaceVarNames', {'meB'}, ...
                'FileVarNames', {anovaMeID});
            
            
        tau = corr([meA.factorA.mean meB.factorA.mean], 'type', 'Kendall');
        tau = tau(2);
        fprintf('  - Kendall''s tau correlation: %5.4f\n', tau);
            
        currentFigure = figure('Visible', 'off');
        
            %--------------------------------------------------------------
            % topic/system on whole corpus
            xTick = 1:length(meA.system);
            data.mean = meA.factorA.mean.';
            data.ciLow = meA.factorA.ci(:, 1).';
            data.ciHigh = meA.factorA.ci(:, 2).';        

            plot(xTick, data.mean, 'Color', rgb('Black'), 'LineWidth', 3, 'LineStyle', '--');

            hold on

            hFill = fill([xTick fliplr(xTick)],[data.ciHigh fliplr(data.ciLow)], rgb('Black'), ...
                        'LineStyle', 'none', 'EdgeAlpha', 0.25, 'FaceAlpha', 0.25);

            % send the fill to back
            uistack(hFill, 'bottom');

            % Exclude fill from legend
            set(get(get(hFill, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle','off');

                    
            %--------------------------------------------------------------
            % topic/system on sub-corpora
            xTick = 1:length(meB.system);
            data.mean = meB.factorA.mean.';
            data.ciLow = meB.factorA.ci(:, 1).';
            data.ciHigh = meB.factorA.ci(:, 2).';        

            plot(xTick, data.mean, 'Color', rgb('FireBrick'), 'LineWidth', 3);

            hold on

            hFill = fill([xTick fliplr(xTick)],[data.ciHigh fliplr(data.ciLow)], rgb('FireBrick'), ...
                        'LineStyle', 'none', 'EdgeAlpha', 0.25, 'FaceAlpha', 0.25);

            % send the fill to back
            uistack(hFill, 'bottom');

            % Exclude fill from legend
            set(get(get(hFill, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle','off');

          
            legend({sprintf('Model of eq. 1 on whole collection'), ... 
                   sprintf('Models of eq. 1 and 2 on sub-corpora')}, ...
                   'Interpreter', 'latex');
            
            ax1 = gca;
            ax1.TickLabelInterpreter = 'latex';
            ax1.FontSize = 24;

            ax1.XTick = [];
            ax1.XTickLabel = [];
            %ax1.XTickLabelRotation = 90;

            ax1.XLabel.Interpreter = 'latex';
            ax1.XLabel.String = sprintf('\\texttt{%s} Systems', trackID);

            ax1.YLabel.Interpreter = 'latex';
            ax1.YLabel.String = sprintf('%s Marginal Mean', EXPERIMENT.measure.(mid).acronym);
            
            ax1.XLim = [0 length(meA.system)];
            ax1.YLim = [0 ax1.YLim(2)];

            title(sprintf('Correlation between rankings of systems on whole collection and sub-corpora $\\tau$ = %5.4f', tau), 'FontSize', 24, 'Interpreter', 'latex');            
            
        currentFigure.PaperPositionMode = 'auto';
        currentFigure.PaperUnits = 'centimeters';
        currentFigure.PaperSize = [42 17];
        currentFigure.PaperPosition = [1 1 40 15];

        figureID = EXPERIMENT.pattern.identifier.anova.me(TAG, mid, splitID, trackID);

        print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(trackID, figureID));

        close(currentFigure)
    
    end; % measure
    
       
    fprintf('\n\n######## Total elapsed time for plotting %s main and interaction effects on track %s (%s): %s ########\n\n', ...
           TAG, ...
           EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
       
end

